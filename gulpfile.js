const gulp = require('gulp');
const del = require('del');
const serve = require('gulp-serve');

const babel = require('gulp-babel');
const sass = require('gulp-sass');
const htmlmin = require('gulp-htmlmin');

const cssmin = require('gulp-cssmin');
const uglify = require('gulp-uglify');
const cachebust = require('gulp-cache-bust');

const bump = require('gulp-bump');
const project = require('./package.json');
const sitedeploy = require('gulp-bitbucket-site-deploy');

const commandline_args = require('command-line-args');

const paths = { src: './site', out: './dist', assets: '/assets' };

/**
 * Clean
 */
gulp.task('clean', () => del([paths.out], { force: true }));

/**
 * Move assets tasks
 */
gulp.task('html', ['clean'], () =>
	gulp
		.src(`${paths.src}/*.html`)
		.pipe(htmlmin({ collapseWhitespace: true }))
		.pipe(gulp.dest(paths.out))
);
gulp.task('styles', ['clean'], () =>
	gulp
		.src(`${paths.src}${paths.assets}/scss/*.scss`)
		.pipe(sass())
		.pipe(gulp.dest(`${paths.out}${paths.assets}/css`))
);
gulp.task('scripts', ['clean'], () =>
	gulp
		.src(`${paths.src}${paths.assets}/js/*.js`)
		.pipe(babel({ presets: ['@babel/preset-env'] }))
		.pipe(gulp.dest(`${paths.out}${paths.assets}/js`))
);

/**
 * Deployment Tasks
 */
gulp.task('bump', () => {
	const args = commandline_args([
		{ name: 'version', alias: 'v', type: String },
		{ name: 'type', alias: 't', type: String, defaultValue: 'patch' }
	]);
	const opts = args.version ? { version: args.version } : { type: args.type };
	gulp
		.src('./package.json')
		.pipe(bump(opts))
		.pipe(gulp.dest('./'));
});

gulp.task('site-deploy', ['default'], cb => {
	sitedeploy({
		username: 'iveseenthedark',
		version: project.version,
		source: `${paths.out}`,
		delete_paths: ['/index.html', '/assets/**']
	});
});

/**
 * Production Tasks
 */
gulp.task('build', ['html', 'scripts', 'styles']);
gulp.task('minify', ['minify-css', 'minify-js']);
gulp.task('minify-css', ['html', 'scripts', 'styles'], () =>
	gulp
		.src(`${paths.out}${paths.assets}/css/*.css`)
		.pipe(cssmin())
		.pipe(gulp.dest(`${paths.out}${paths.assets}/css`))
);
gulp.task('minify-js', ['html', 'scripts', 'styles'], () =>
	gulp
		.src(`${paths.out}${paths.assets}/js/*.js`)
		.pipe(uglify())
		.pipe(gulp.dest(`${paths.out}${paths.assets}/js`))
);
gulp.task('cachebust', ['html', 'scripts', 'styles'], () =>
	gulp
		.src(`${paths.out}/*.html`)
		.pipe(cachebust())
		.pipe(gulp.dest(`${paths.out}`))
);

/**
 * Development Tasks
 */
gulp.task('serve', serve({ root: [paths.out], port: 9090 }));
gulp.task('watch', ['build', 'serve'], () => {
	gulp.watch(`${paths.src}/**/*.{html,js,scss}`, ['build']);
});

gulp.task('default', ['build', 'minify', 'cachebust']);
